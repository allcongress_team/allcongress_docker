# Notes

## First time environment setup
https://github.com/blinkreaction/drude/blob/develop/docs/drude-env-setup.md

## Install local dev steps


1. Clone this repo into the Projects directory

    ```
    git clone git@bitbucket.org:allcongress_team/allcongress_dockerized.git
    cd allcongress_dockerized
    ```

2. Initial drupal website installation

    ```
    dsh init
    ```

3. Import saved DB export

    ```
    cd docroot
    dsh mysql-import sites/default/files/dbexport.sql
    ```

4. Use drupal cli  

    ```
    dsh bash
    ```

5. Add `192.168.10.10  drupal7.drude` to your hosts file

6. Point your browser to

    ```
    http://allcongress.drude
    ```

7. Remember to export DB to commit changes at DB

    ```
    dsh bash
    cd docroot
    drush sql-dump > ../drupal_install/dbexport.sql
    ```


## Theme setup

- at HOST machine go to `sites/all/themes` folder & clone allcongress_squared repo
- at allcongress_squared folder `cd .npm/`
- npm install
- npm install gulp -g
- gulp
-



## Run local dev environment


### start & stop application*

  ```
  dsh start
  ```

  and
  ```
  dsh stop
  ```

  or stop all running containers
  ```
  docker stop $(docker ps -a -q)
  ```



### Reset Vagrant VM when Networking errors

  ```
  cd .. {at projects folder}
  vagrant reload
  ```
